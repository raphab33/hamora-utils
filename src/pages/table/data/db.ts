export const columns = [
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
    sorter: true,
  },
  {
    title: "Age",
    dataIndex: "age",
    key: "age",
    sorter: true,
  },
  {
    title: "Address",
    dataIndex: "address",
    key: "address",
    sorter: true,
  },
];

export const dataSource = [
  {
    key: "1",
    name: "Mike",
    age: 32,
    address: "10 Downing Street",
  },
  {
    key: "2",
    name: "John",
    age: 42,
    address: "10 Downing Street",
  },
  {
    key: "3",
    name: "Lucas",
    age: 32,
    address: "10 Downing Street",
  },
  {
    key: "4",
    name: "Diana",
    age: 42,
    address: "10 Downing Street",
  },
  {
    key: "5",
    name: "Lucas",
    age: 32,
    address: "10 Downing Street",
  },
  {
    key: "6",
    name: "Lucas",
    age: 32,
    address: "10 Downing Street",
  },
  {
    key: "7",
    name: "Lucas",
    age: 32,
    address: "10 Downing Street",
  },
  {
    key: "8",
    name: "Lucas",
    age: 32,
    address: "10 Downing Street",
  },
  {
    key: "9",
    name: "Lucas",
    age: 32,
    address: "10 Downing Street",
  },
  {
    key: "10",
    name: "Lucas",
    age: 32,
    address: "10 Downing Street",
  },
  {
    key: "11",
    name: "Lucas",
    age: 32,
    address: "10 Downing Street",
  },
  {
    key: "12",
    name: "Lucas",
    age: 32,
    address: "10 Downing Street",
  },
  {
    key: "13",
    name: "Lucas",
    age: 32,
    address: "10 Downing Street",
  },
  {
    key: "14",
    name: "Lucas",
    age: 32,
    address: "10 Downing Street",
  },
  {
    key: "15",
    name: "Lucas",
    age: 32,
    address: "10 Downing Street",
  },
  {
    key: "16",
    name: "Lucas",
    age: 32,
    address: "10 Downing Street",
  },
  {
    key: "17",
    name: "Lucas",
    age: 32,
    address: "10 Downing Street",
  },
  {
    key: "18",
    name: "Lucas",
    age: 32,
    address: "10 Downing Street",
  },
];
