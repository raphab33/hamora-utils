import styled from "styled-components";

export const Container = styled.div`
  padding: 16px;
  max-width: 800px;
  margin: 0 auto;
  margin-top: 32px;
  background-color: #fff;
  border-radius: 8px;
  box-shadow: 0 0 8px rgba(0, 0, 0, 0.1);
`;
