import * as S from "./styles";
import { Button, Divider, Input, Space, Table } from "antd";
import { columns, dataSource } from "./data/db";
import { FileExcelOutlined, FilePdfOutlined } from "@ant-design/icons";
import { useState } from "react";

const TablePage = () => {
  const [dataSourceView, setDataSourceView] = useState<any[]>([...dataSource]);

  const handleSearch = (text: string) => {
    const data = dataSource.filter((item) => {
      return item.name.toLowerCase().includes(text.toLowerCase());
    });

    setDataSourceView(data);
  };

  return (
    <S.Container>
      <Space style={{ marginBottom: 16 }}>
        <Button onClick={() => {}}>
          <FilePdfOutlined />
          PDF
        </Button>
        <Button onClick={() => {}}>
          <FileExcelOutlined />
          Excel
        </Button>
        <Input
          placeholder="Pesquisar..."
          onChange={(event) => handleSearch(event.target.value)}
        />
      </Space>
      <Divider />
      <Table columns={columns} dataSource={dataSourceView} />
    </S.Container>
  );
};

export default TablePage;
